import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

export default function App() {
  const [value, setValue] = React.useState(0)

  return (
    <ScrollView style={styles.main}>
      <View style={styles.container}>
        <Text style={styles.text}>Native Todo App version 1.0.{value}</Text>

        <StatusBar style="auto" />
      </View>

      <View style={styles.buttons}>
        <Text style={styles.btn} onPress={() => setValue(prev => prev - 1)}>-</Text>
        <Text style={styles.btn} onPress={() => setValue(prev => prev + 1)}>+</Text>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: "brown",
    display: "flex",
  },
  flex: {
    display: "flex",
    flexDirection: "row",
    flex: 1,
  }
  ,
  container: {
    flex: 1,
    marginTop: 40,
    minHeight: 50,
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: "#fff"
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
    justifyContent: "space-evenly",
    width: "50%",
    marginTop: 30,
    alignSelf: "center",
  },
  btn: {
    borderBlockColor: "gold",
    color: "#fff",
    fontSize: 20,
    backgroundColor: "grey",
    height: "auto",
    paddingTop: 1,
    paddingBottom: 1,
    paddingRight: 10,
    paddingLeft: 10,
  }
});
